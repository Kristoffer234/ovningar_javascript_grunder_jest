
/***
 *       _____  __         _                                                  
 *      / ___/ / /_ _____ (_)____   ____ _                                    
 *      \__ \ / __// ___// // __ \ / __ `/______                              
 *     ___/ // /_ / /   / // / / // /_/ //_____/                              
 *    /____/ \__//_/   /_//_/ /_/ \__, /                                      
 *        __  ___               _/____/          __        __   _             
 *       /  |/  /____ _ ____   (_)____   __  __ / /____ _ / /_ (_)____   ____ 
 *      / /|_/ // __ `// __ \ / // __ \ / / / // // __ `// __// // __ \ / __ \
 *     / /  / // /_/ // / / // // /_/ // /_/ // // /_/ // /_ / // /_/ // / / /
 *    /_/  /_/ \__,_//_/ /_//_// .___/ \__,_//_/ \__,_/ \__//_/ \____//_/ /_/ 
 *                            /_/                                             
 */

// "Vilket är den funktionella programmerarens favoritdjur? Lamm, duh!"

/***
 *       _  __ _         _      __
 *      / |/ /(_)_  __ _(_)_  <  /
 *     /    // /| |/ // _ `/  / / 
 *    /_/|_//_/ |___/ \_,_/  /_/  
 *                                
 */


//                              OBS!

// Det här är i hög grad en träning också i att läsa och vänja sig med dokumentation
// Det finns gott om metoder kopplade till strängar, och många kommer man att behöva titta upp när man programmerar
// Besök https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/
// Under sidomenyn "Methods" finns ett uppslag med metoder kopplade till strängar
// Kika gärna runt bland dem om du funderar på hur du ska lösa de olika uppgifterna, och se om det
// finns någon metod där som kan tänkas göra det du behöver göra för att läsa uppgiften

const uppg1 = () => {

    const word = "Hej"

    // Gör word till enbart stora bokstäver ("Hej" -> "HEJ")

    return word.toUpperCase() // returnera en variabel som håller ditt resultat

}
// Uncomment below to run manually
// uppg1()


const uppg2 = () => {

    const word = "Tjena"

    // Gör word till enbart små bokstäver ("Tjena" -> "tjena")

    return word.toLowerCase() // returnera en variabel som håller ditt resultat
}
// Uncomment below to run manually
// uppg2()

const uppg3 = (word) => {

    // Räkna antalet bokstäver i ordet

    return word.length // returnera en variabel som håller ditt resultat
}
// Uncomment below to run manually
// uppg3("Bil")


const uppg4 = (word) => {

    return word.endsWith("a") // returnera true eller false beroende på om word slutar med bokstaven "a"
}
// Uncomment below to run manually
// uppg4("mygga")


const uppg5 = (word) => {

    return word.startsWith("a") // returnera true eller false beroende på om word börjar med bokstaven "a"
}
// Uncomment to run manually
// uppg5("arbetskamrat")



const uppg6 = (sentence) => {

    return sentence.includes("cykel") // returnera true eller false beroende på om sentence innehåller "cykel"
}
// Uncomment to run manually
// uppg6("Jag har en bil, en cykel och en båt")


/***
 *        _   __ _          _       ___ 
 *       / | / /(_)_   __ _(_) _   |__ \
 *      /  |/ // /| | / // __ `/   __/ /
 *     / /|  // / | |/ // /_/ /   / __/ 
 *    /_/ |_//_/  |___/ \__,_/   /____/ 
 *                                      
 */


const uppg7 = (sentence) => {

    // returnera en ny mening där alla "nåt" i sentence har ersatts med "något"

    return sentence.replaceAll("nåt", "något")

}
// Uncomment to run manually
// uppg7("Jag har handlat nåt i affären, men det är nåt som är hemligt fram tills din födelsedag")

const uppg8 = (number) => {

    const word = "Hej"

    // Skapa en ny sträng som håller word repeterad så många gånger som number är (T.ex. om number är 3, "Hej" -> "HejHejHej")

    return word.repeat(number) // returnera en variabel som håller ditt resultat

}
// Uncomment to run manually
// uppg8(5)

const uppg9 = () => {

    const sentence = "Det här är en exempelmening"

    // Splitta upp meningen ovan till en lista, där varje enskilt ord blir ett eget element ("["Det här är en exempelmening" -> ["Det","här","är","en","exempelmening"])

    return sentence.split(" ") // returnera en variabel som håller ditt resultat

}
// Uncomment to run manually
// uppg9("Gunnar")

const uppg10 = (sentence) => {

    return sentence.slice(0, 10) // returnera de 10 första bokstäverna i sentence


}
// Uncomment to run manually
// uppg10("Det här är en exempelmening")


/***
 *        ____                                                          _  ____ __             
 *       / __ ) ____   ____   __  __ _____ __  __ ____   ____   ____ _ (_)/ __// /_ ___   _____
 *      / __  |/ __ \ / __ \ / / / // ___// / / // __ \ / __ \ / __ `// // /_ / __// _ \ / ___/
 *     / /_/ // /_/ // / / // /_/ /(__  )/ /_/ // /_/ // /_/ // /_/ // // __// /_ /  __// /    
 *    /_____/ \____//_/ /_/ \__,_//____/ \__,_// .___// .___/ \__, //_//_/   \__/ \___//_/     
 *                                            /_/    /_/     /____/                            
 */


const uppg11 = (number) => {

    // En vanlig kodtest-uppgift är att konvertera ett vanligt tal i vårt vanliga decimala talsystem, till romerska siffror (Som en sträng, "III", "IV" etc.)
    // Du hittar en tabell här: https://www.javatpoint.com/roman-number-1-to-100
    // En lösning involverar enbart Stringmanipulation m. metoderna repeat och replace- se om du kan hitta den
    // Din lösning skall ha stöd för (minst) värden upp till 50;

    const base = "I";

    return base
        .repeat(number)
        .replaceAll('IIIII', 'V')
        .replaceAll('IIII', 'IV')
        .replaceAll('VV', 'X')
        .replaceAll('VIV', 'IX')
        .replaceAll('XXXXX', 'L')
        .replaceAll('XXXX', 'XL')
    // returnera en variabel som håller ditt resultat

}
// Uncomment to run manually
// uppg11(13)


const uppg12 = (sentence) => {

    // I en del språk (t.ex. tyska) versaliserar man alla substantiv (t.ex. "Ich habe ein Auto geschenkt", där "Auto" betyder bil och därför har stor första bokstav)
    // För varje exempelmening du får in som innehåller ordet "en" eller "ett", versalisera första bokstaven i nästkommande ord (vi låtsas inte om att adjektivt o.dyl. finns)

    const wordList = sentence.split(" ");
    let capitalizeNextWord = false

    for (let i = 0; i < wordList.length; i++) {

        if (capitalizeNextWord) {
            console.log("före", wordList[i])
            wordList[i] = wordList[i].charAt(0).toUpperCase() + wordList[i].slice(1)
            capitalizeNextWord = false
        }

        if (wordList[i] === "en" || wordList[i] === "ett") {
            capitalizeNextWord = true
        }
    }

    return wordList.join(" ") // returnera en variabel som håller ditt resultat

}
// Uncomment to run manually
// uppg12("Jag har köpt mig en bil, en båt och ett hus")




module.exports = {
    uppg1, uppg2, uppg3, uppg4, uppg5, uppg6, uppg7, uppg8, uppg9, uppg10, uppg11, uppg12
}




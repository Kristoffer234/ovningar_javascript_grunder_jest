
/***
 *        ______               __    __   _                          
 *       / ____/__  __ ____   / /__ / /_ (_)____   ____   ___   _____
 *      / /_   / / / // __ \ / //_// __// // __ \ / __ \ / _ \ / ___/
 *     / __/  / /_/ // / / // ,<  / /_ / // /_/ // / / //  __// /    
 *    /_/     \__,_//_/ /_//_/|_| \__//_/ \____//_/ /_/ \___//_/     
 *                                                                   
 */

// "Vilket är den funktionella programmerarens favoritdjur? Lamm, duh!"

/***
 *       _  __ _         _      __
 *      / |/ /(_)_  __ _(_)_  <  /
 *     /    // /| |/ // _ `/  / / 
 *    /_/|_//_/ |___/ \_,_/  /_/  
 *                                
 */

const uppg1 = (a, b) => {

    /***
     * Definiera en funktion sum på formatet:
     * 
     * function funktionsnamn(x, y){
     *     // Ev. kod
     *     // Ev. kod
     *     return something
     * }
     * 
     * 
     * Funktionen skall adderar ihop första och andra parametern, returnera summan
     **/

    function sum(x, y) {
        return x + y;
    }

    // Anropa din funktion sum med argumenten a & b och lagra det returnerade värdet i en variabel 
    // console.log:a ut det returnerade värdet
    const result = sum(a, b)
    console.log(result)

    return sum // ersätt null med namnet på din funktion
}
// Uncomment below to run manually
// uppg1(1, 3)


const uppg2 = (a, b) => {

    /***
     * Definiera en funktion multiply på formatet:
     * 
     * const funktionsnamn = function(x, y){
     *     // Ev. kod
     *     // Ev. kod
     *     return something
     * }
     * 
     * 
     * Funktionen skall multiplicera första och andra parametern, returnera resultatet
     **/
    const multiply = function (x, y) {
        return x * y
    }

    // Anropa din funktion multiply med argumenten a & b, och lagra det returnerade värdet i en variabel
    // console.log:a ut det returnerade värdet
    const result = multiply(a, b)
    console.log(result)

    return multiply // ersätt null med namnet på din funktion

}
// Uncomment below to run manually
// uppg2()

const uppg3 = (a, b) => {

    /***
     * Definiera en funktion subtract på formatet:
     * 
     * const function = (x, y) => {
     *     // Ev. kod
     *     // Ev. kod
     *     return something
     * }
     * 
     * alt. kortformen:
     * 
     * const function = (x, y) => uttryck-att-evaluera-och-returnera
     * 
     * 
     * Funktionen skall subtrahera andra parameterns värde från den första parameterns värde, returnera resultatet
     **/
    const subtract = (x, y) => {
        return x - y
    }

    // Anropa din funktion subtract med argumenten a & b, och lagra det returnerade värdet i en variabel
    // console.log:a ut det returnerade värdet
    const result = subtract(a, b)
    console.log(result)

    return subtract // ersätt null med namnet på din funktion


}
// Uncomment below to run manually
// uppg3(5, 10)


const uppg4 = (a, b) => {

    // Definera en funktion på valfritt format 
    // Funktionen skall ta in jämföra två nummer, returnera det med högst värde
    const returnLargestNumber = (x, y) => {
        return x > y ? x : y;
    }

    return returnLargestNumber // ersätt null med namnet på din funktion

}
// Uncomment below to run manually
// uppg4(3, 6)


const uppg5 = (a) => {

    // Definera en funktion på valfritt format 
    // Funktionen skall ta in ett nummer, och multiplicera det med sig själv
    const kvadrering = x => x * x

    return kvadrering // ersätt null med namnet på din funktion

}
// Uncomment to run manually
// uppg5(5)



const uppg6 = (lang, name) => {

    // Definera en funktion på valfritt format 
    // Funktionen skall ta in två paramentrar
    // Ena parametern representerar ett språk, och kan ha värdena 'sv' eller 'en'
    // Andra parametern representerar ett namn
    // funktionen skall sätta ihop och returnera en sträng, "Hej <namn>!" alt. "Hello <namn>!", beroende på språk
    const greeter = (lang, name) => {
        if (lang === 'sv') {
            return `Hej ${name}!`
        } else {
            return `Hello ${name}!`
        }
    }

    return greeter // ersätt null med namnet på din funktion

}
// Uncomment to run manually
// uppg6('sv', 'gunnar')

const uppg7 = () => {

    // I koden nedan, identifiera parametern
    let name = "Gunnar";

    function wordLength(word) {
        return word.length
    }

    const numberOfCharacters = wordLength(name)

    return "word" // ersätt null med namnet på parametern


}
// Uncomment to run manually
// uppg7()

const uppg8 = () => {

    // I koden nedan, identifiera namnet på funktionen
    let name = "Gunnar";

    function wordLength(word) {
        return word.length
    }

    const numberOfCharacters = wordLength(name)

    return "wordLength" // ersätt null med namnet på funktionen


}
// Uncomment to run manually
// uppg8()

const uppg9 = () => {

    // I koden nedan, identifiera returtypen
    let name = "Gunnar";

    function wordLength(word) {
        return word.length
    }

    const numberOfCharacters = wordLength(name)

    return "number" // ersätt null med namnet på returtypen


}
// Uncomment to run manually
// uppg9()

const uppg10 = () => {

    // I koden nedan, identifiera värdet på argumentet vid funktionsanropet
    let name = "Gunnar";

    function wordLength(word) {
        return word.length
    }

    const numberOfCharacters = wordLength(name)

    return "Gunnar" // ersätt null med värdet på argumentet vid funktionsanropet


}
// Uncomment to run manually
// uppg10()


/***
 *        _   __ _          _       ___ 
 *       / | / /(_)_   __ _(_) _   |__ \
 *      /  |/ // /| | / // __ `/   __/ /
 *     / /|  // / | |/ // /_/ /   / __/ 
 *    /_/ |_//_/  |___/ \__,_/   /____/ 
 *                                      
 */

const uppg11 = (playerMove, computerMove) => {

    // Definiera en funktion, som avgör vinnaren i en sten-sax-påse-runda
    // Funktionen skall ta en parameter som representerar ett drag från en spelare
    // Och en andra parameter som på motsvarande vis representerar ett drag från datorn
    // Giltiga värden in är "sten", "sax", "påse". 
    // Returnera "lika", "du vann" eller "datorn vann".

    const decideWinner = (playerMove, computerMove) => {

        switch (playerMove) {
            case 'sten':
                if (computerMove === "sten") {
                    return "lika"
                }
                if (computerMove === "sax") {
                    return "du vann"
                }
                if (computerMove === "påse") {
                    return "datorn vann"
                }
            case 'sax':
                if (computerMove === "sten") {
                    return "datorn vann"
                }
                if (computerMove === "sax") {
                    return "lika"
                }
                if (computerMove === "påse") {
                    return "datorn vann"
                }
            case 'påse':
                if (computerMove === "sten") {
                    return "du vann"
                }
                if (computerMove === "sax") {
                    return "datorn vann"
                }
                if (computerMove === "påse") {
                    return "lika"
                }
        }
    }

    return decideWinner // ersätt null med namnet på din funktion

}
// Uncomment to run manually
// uppg11("sten", "påse")

const uppg12 = (list) => {

    // Definiera en funktion, som tar in en lista och returnerar en reverserad / vänd lista
    // (Så att första elementet ligger sist, näst första elementet näst sist etc.)
    const reverseList = function (lista) {
        let newList = []
        for (let i = 0; i < lista.length; i++) {
            newList[lista.length - 1 - i] = lista[i]
        }
        return newList;
    }

    reverseList(list)

    return reverseList // ersätt null med namnet på din funktion

}
// Uncomment to run manually
// uppg12([1, 5, 3, 10])




/***
 *        ____                                                          _  ____ __             
 *       / __ ) ____   ____   __  __ _____ __  __ ____   ____   ____ _ (_)/ __// /_ ___   _____
 *      / __  |/ __ \ / __ \ / / / // ___// / / // __ \ / __ \ / __ `// // /_ / __// _ \ / ___/
 *     / /_/ // /_/ // / / // /_/ /(__  )/ /_/ // /_/ // /_/ // /_/ // // __// /_ /  __// /    
 *    /_____/ \____//_/ /_/ \__,_//____/ \__,_// .___// .___/ \__, //_//_/   \__/ \___//_/     
 *                                            /_/    /_/     /____/                            
 */

const uppg13 = (number) => {

    // Definiera en funktion, som tar in ett argument och returnerar dess kvadratrot
    // Du får inte använda färdiga hjälpmetoder i JS, såsom Math.sqrt().
    // Enbart positiva heltal (inkl. rötter) behöver hanteras

    const sqrt = (numb) => {
        for (let i = 0; i * i <= numb; i++) {
            if (i * i === numb)
                return i;
        }
    }

    return sqrt // ersätt null med namnet på din funktion

}
// Uncomment to run manually
// uppg13(25)


module.exports = {
    uppg1, uppg2, uppg3, uppg4, uppg5, uppg6, uppg7, uppg8, uppg9, uppg10, uppg11, uppg12, uppg13
}





/***
 *        ___                                     
 *       /   |  ______________ ___  __            
 *      / /| | / ___/ ___/ __ `/ / / /_____       
 *     / ___ |/ /  / /  / /_/ / /_/ /_____/       
 *    /_/  |_/_/  /_/   \__,_/\__, /   __         
 *       ____ ___  ___  / /__/____/___/ /__  _____
 *      / __ `__ \/ _ \/ __/ __ \/ __  / _ \/ ___/
 *     / / / / / /  __/ /_/ /_/ / /_/ /  __/ /    
 *    /_/ /_/ /_/\___/\__/\____/\__,_/\___/_/     
 *                                                
 */

// "Strängarna borde testa att vara lite mer källkritiska."

/***
 *       _  __ _         _      __
 *      / |/ /(_)_  __ _(_)_  <  /
 *     /    // /| |/ // _ `/  / / 
 *    /_/|_//_/ |___/ \_,_/  /_/  
 *                                
 */


const uppg1 = () => {

    const fruit = "banana"

    const fruits = ["apple", "orange"]

    // Lägg till fruit (bananen) sist i arrayen fruits ( -> ["apple", "orange", "banana")

    fruits.push(fruit)

    return fruits;

}
// Uncomment below to run manually
// uppg1()


// ---------------------------------------------------------------------- //


const uppg2 = () => {

    const fruit1 = "pineapple"
    const fruit2 = "banana"

    const fruits = ["apple", "orange"]

    // Lägg till fruit1 och fruit2 först i fruits ( -> ["pineapple", "banana", "apple", "orange"])
    fruits.unshift(fruit1, fruit2)

    return fruits;
}
// Uncomment below to run manually
// uppg2()


// ---------------------------------------------------------------------- //


const uppg3 = () => {

    const fruits = ["banana", "apple", "orange"]

    // ta bort "orange" ur listan

    fruits.pop()

    return fruits;
}
// Uncomment below to run manually
// uppg3()


// ---------------------------------------------------------------------- //


const uppg4 = () => {

    const fruits = ["banana", "apple", "orange"]

    // ta bort "banana" ur listan

    fruits.shift()

    return fruits;

}
// Uncomment below to run manually
// uppg4()


// ---------------------------------------------------------------------- //


const uppg5 = () => {

    const fruits = ["banana", "apple", "orange"]

    // ta bort "apple" ur listan

    fruits.splice(1, 1)

    return fruits;
}
// Uncomment to run manually
// uppg5()


// ---------------------------------------------------------------------- //


const uppg6 = () => {

    const fruits = ["banana", "apple", "orange"]

    // ta fram vilken position (index) som "orange" befinner sig på i listan
    let index = fruits.indexOf("orange")

    return index;

}
// Uncomment to run manually
// uppg6()


// ---------------------------------------------------------------------- //


const uppg7 = () => {

    const fruits = ["banana", "apple", "orange"]

    // vänd på listan ( -> ["orange", "apple", "banana"])
    fruits.reverse()

    return fruits;

}
// Uncomment to run manually
// uppg7()


// ---------------------------------------------------------------------- //


const uppg8 = () => {

    const fruits = ["banana", "apple", "orange"]

    // skriv ut alla frukterna i consolen med hjälp av .forEach()

    fruits.forEach((fruit) => console.log(fruit))


}
// Uncomment to run manually
// uppg8()


// ---------------------------------------------------------------------- //


const uppg9 = () => {

    const fruits = ["banan", "äpple", "citron"]

    // Lagra listans frukter i en variabel,
    // som håller löpande text på formatet "Idag har jag ätit [frukt1], [frukt2], [frukt3]"


    return `Idag har jag ätit ${fruits.join(", ")}`

}
// Uncomment to run manually
// uppg9()



/***
 *        _   __ _          _       ___ 
 *       / | / /(_)_   __ _(_) _   |__ \
 *      /  |/ // /| | / // __ `/   __/ /
 *     / /|  // / | |/ // /_/ /   / __/ 
 *    /_/ |_//_/  |___/ \__,_/   /____/ 
 *                                      
 */



const uppg10 = () => {

    const employees = [
        { name: "Alice", age: 52, department: "IT" },
        { name: "Bob", age: 24, department: "HR" },
        { name: "Charlie", age: 32, department: "Sales" }
    ]

    // skriv ut namnet (och enbart namnet) på alla anställda i consolen med hjälp av .forEach()
    employees.forEach(employees => console.log(employees.name))

}
// Uncomment to run manually
// uppg10()


// ---------------------------------------------------------------------- //


/***
 *        ____                                                          _  ____ __             
 *       / __ ) ____   ____   __  __ _____ __  __ ____   ____   ____ _ (_)/ __// /_ ___   _____
 *      / __  |/ __ \ / __ \ / / / // ___// / / // __ \ / __ \ / __ `// // /_ / __// _ \ / ___/
 *     / /_/ // /_/ // / / // /_/ /(__  )/ /_/ // /_/ // /_/ // /_/ // // __// /_ /  __// /    
 *    /_____/ \____//_/ /_/ \__,_//____/ \__,_// .___// .___/ \__, //_//_/   \__/ \___//_/     
 *                                            /_/    /_/     /____/                            
 */



const uppg11 = (fruits) => {

    // Om det finns en eller flera "banana" i listan, plocka bort den / dem

    while (fruits.indexOf("banana") >= 0) {
        const index = fruits.indexOf("banana")
        fruits.splice(index, 1)
    }

    return fruits

}
// Uncomment to run manually
// uppg11(["banana", "apple", "orange"])


module.exports = {
    uppg1, uppg2, uppg3, uppg4, uppg5, uppg6, uppg7, uppg8, uppg9, uppg10, uppg11
}





/***
 *       ______                   __ _  __   _                       __     
 *      / ____/____   ____   ____/ /(_)/ /_ (_)____   ____   ____ _ / /_____
 *     / /    / __ \ / __ \ / __  // // __// // __ \ / __ \ / __ `// // ___/
 *    / /___ / /_/ // / / // /_/ // // /_ / // /_/ // / / // /_/ // /(__  ) 
 *    \____/ \____//_/ /_/ \__,_//_/ \__//_/ \____//_/ /_/ \__,_//_//____/  
 *                                                                          
 */

// "Gunnar, skulle du kunna kila förbi affären och köpa ett par baguetter? Och om de har ägg, köp ett dussin vettja!"
//
//  ---
//
//  Gunnar: "De hade ägg! Här är bröden: 🥖🥖🥖🥖🥖🥖🥖🥖🥖🥖🥖🥖"

/***
 *       _  __ _         _      __
 *      / |/ /(_)_  __ _(_)_  <  /
 *     /    // /| |/ // _ `/  / / 
 *    /_/|_//_/ |___/ \_,_/  /_/  
 *                                
 */

const uppg1 = (number) => {

    // Om 'number' är 18 eller större, printa ut "Du är myndig"

    if (number >= 18) {
        console.log("Du är myndig")
    }


}
// Uncomment below to run manually
// uppg1(18)


const uppg2 = (number) => {


    // Om 'number' är inom åldersspannet 13-19, printa ut "Du är en tonåring"
    if (number >= 13 && number <= 19)
        console.log("Du är en tonåring")


}
// Uncomment below to run manually
// uppg2(18)

const uppg3 = (age) => {

    // Om 'age' är 20 eller större, printa ut "Du är vuxen"
    // Om 'age' är inom åldersspannet 13-19, printa ut "Du är en tonåring"
    // Om 'age' är inom åldersspannet 0-12, printa ut "Du är ett barn"
    // I alla andra fall, skriv ut "ogiltigt värde, kan ej avgöra din ålderskategori"
    // Avslutningsvis, skriv ut "Tack för att du använt åldersbestämmaren 3000" (oavsett värde på age)
    // Fyll på utifrån den existerande strukturen nedan
    if (age >= 20) {
        console.log("Du är vuxen")
    } else if (age >= 13) {
        console.log("Du är en tonåring")
    } else if (age >= 0) {
        console.log("Du är ett barn")
    } else {
        console.log("ogiltigt värde, kan ej avgöra din ålderskategori")
    }
    console.log("Tack för att du använt åldersbestämmaren 3000")


}

// Uncomment below to run manually
// uppg3(5)


const uppg4 = (name) => {

    // Om name är "Gunnar", returnera true
    // Annars, returnera false
    if (name === "Gunnar")
        return true

    return false

}
// Uncomment below to run manually
// uppg4()


const uppg5 = (month) => {

    // Använd ett switch-statement för att printa ut hur många dagar årets första månader har
    // Du kan bortse från skottår
    // (month kan här ha värdena 'jan', 'feb', 'mar', 'apr', 'may', 'jun')
    // om det inte är en månad med något av ovan angivna värden, printa "ogiltigt värde"
    switch (month) {
        case 'jan':
            console.log(31)
            break;
        case 'feb':
            console.log(28)
            break;
        case 'mar':
            console.log(31)
            break;
        case 'apr':
            console.log(30)
            break;
        case 'may':
            console.log(31)
            break;
        case 'jun':
            console.log(30)
            break;
        default:
            console.log("ogiltigt värde")

    }

}
// Uncomment to run manually
// uppg5('jan')



const uppg6 = (age) => {

    // Ersätt null, för att m.h.a. av ternary-operatorerna under returnera "myndig" resp. "omyndig" om villkoret är uppfyllt
    const isMyndig = (age >= 18) ? "myndig" : "omyndig";

    console.log(`Du är ${isMyndig}`)

}
// Uncomment to run manually
// uppg6(8)

/***
 *        _   __ _          _       ___ 
 *       / | / /(_)_   __ _(_) _   |__ \
 *      /  |/ // /| | / // __ `/   __/ /
 *     / /|  // / | |/ // /_/ /   / __/ 
 *    /_/ |_//_/  |___/ \__,_/   /____/ 
 *                                      
 */

const uppg7 = (weekday, hour) => {

    // En butik är öppen 07-21 på vardagar, 09-18 på lördagar och stängd på söndagar
    // Skriv logik som printar ut "öppet" respektive "stängt" beroende på variablerna weekday och hour 
    // (första timmen, dvs 7 respektive 9, räknas som öppen. Sista timmen, 21 & 18, räknas som stängd)
    // ('weekday' kommer kunna hålla värdena 'monday, 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday')
    switch (weekday) {
        case 'monday':
        case 'tuesday':
        case 'wednesday':
        case 'thursday':
        case 'friday':
            if (hour >= 7 && hour < 21) {
                console.log("öppet")
            } else {
                console.log("stängt")
            };
            break;
        case 'saturday':
            if (hour >= 9 && hour < 18) {
                console.log("öppet")
            } else {
                console.log("stängt")
            };
            break;
        case 'sunday':
            console.log("stängt")

    }


}
// Uncomment to run manually
// uppg7("saturday", 15)



const uppg8 = (user, inputedUsername, inputedPassword) => {

    // I user finns attributen username samt password
    // Jämför dessa och returnera "Authorized" om bägge matchar
    // Annars, returnera "UnAuthorized"
    if(user.username === inputedUsername && user.password === inputedPassword){
        return "Authorized"
    }
    else { return "UnAuthorized" }

}
// Uncomment to run manually
// uppg8({ username: "Gunnar", password: "Hemligt" }, "Gunnar", "Hemligt")

/***
 *        ____                                                          _  ____ __             
 *       / __ ) ____   ____   __  __ _____ __  __ ____   ____   ____ _ (_)/ __// /_ ___   _____
 *      / __  |/ __ \ / __ \ / / / // ___// / / // __ \ / __ \ / __ `// // /_ / __// _ \ / ___/
 *     / /_/ // /_/ // / / // /_/ /(__  )/ /_/ // /_/ // /_/ // /_/ // // __// /_ /  __// /    
 *    /_____/ \____//_/ /_/ \__,_//____/ \__,_// .___// .___/ \__, //_//_/   \__/ \___//_/     
 *                                            /_/    /_/     /____/                            
 */

/*
if (player !== null) {
    if (player.isFrozen) {
        return 0
    } else {
        if (!player.isDead) {
            if (player.selectedPrimaryWeapon) {
                return baseAttack * player.selectedPrimaryWeapon.attackModifier
            } else {
                return baseAttack;
            }
        } else {
            return 0;
        }
    }
} else {
    throw new Exception("Player object null or undefined")
}
*/

const uppg9 = (player) => {

    // Använd valfri sökmotor för att finna information om en teknik kallad "Guard Clauses"
    // Nedan är logik för att hantera en spelare i ett spel, som skall utföra en attack
    // Mängden skada som spelaren gör skall sedan returneras
    // Skriv om logiken m.h.a. av s.k. Guard Clauses

    const baseAttack = 20;


    if (player == null)
        throw new Exception("Player object null or undefined")

    if (player.isDead) {
        return 0;
    }

    if (player.isFrozen)
        return 0;

    if (player.selectedPrimaryWeapon)
        return baseAttack * player.selectedPrimaryWeapon.attackModifier

    return baseAttack;

}
// Uncomment to run manually
// uppg9("Gunnar")


module.exports = {
    uppg1, uppg2, uppg3, uppg4, uppg5, uppg6, uppg7, uppg8, uppg9
}